package de.mehrer.prodlist;

import de.abas.erp.api.gui.MenuBuilder;
import de.abas.erp.axi.event.EventException;
import de.abas.erp.axi.screen.ScreenControl;
import de.abas.erp.axi2.EventHandlerRunner;
import de.abas.erp.axi2.annotation.ButtonEventHandler;
import de.abas.erp.axi2.annotation.EventHandler;
import de.abas.erp.axi2.event.ButtonEvent;
import de.abas.erp.axi2.type.ButtonEventType;
import de.abas.erp.common.type.enums.EnumFilingModeSelection;
import de.abas.erp.common.type.enums.EnumWorkOrderType;
import de.abas.erp.db.*;
import de.abas.erp.db.exception.CommandException;
import de.abas.erp.db.infosystem.custom.owfe.ProductionCentre;
import de.abas.erp.db.schema.purchasing.Reservations;
import de.abas.erp.db.schema.workorder.CompletionConfirmations;
import de.abas.erp.db.schema.workorder.CompletionConfirmationsEditor;
import de.abas.erp.db.schema.workorder.WorkOrders;
import de.abas.erp.db.schema.workorder.WorkOrdersEditor;
import de.abas.erp.db.selection.Conditions;
import de.abas.erp.db.selection.SelectionBuilder;
import de.abas.erp.jfop.rt.api.annotation.RunFopWith;
import de.mehrer.abaserp.utils.CompletionConfirmationsUtils;
import de.mehrer.abaserp.utils.RecordUtils;
import de.mehrer.abaserp.utils.ReservationsUtils;
import de.mehrer.abaserp.utils.WorkordersUtils;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@EventHandler(head = ProductionCentre.class, row = ProductionCentre.Row.class)

@RunFopWith(EventHandlerRunner.class)

// java:de.mehrer.prodlist.ProdlistEventHandler@i0139
public class ProdlistEventHandler {

    @ButtonEventHandler(field = "yrmloe", type = ButtonEventType.AFTER, table = true)
    public void yrmloeAfter(ButtonEvent event, ScreenControl screenControl, DbContext ctx,
                            ProductionCentre head, ProductionCentre.Row row) throws EventException {

        try {

            // Check icon
            // replace - wieder herstellen
            // replace_disabled - rückgängig machen
            final String iconYrmloe = row.getString("yrmloe");
            final boolean iconReplace = iconYrmloe.equals("icon:replace");
            final boolean iconReplaceDisabled = iconYrmloe.equals("icon:replace_disabled");
            if (!iconReplace && !iconReplaceDisabled)
                return;

            if (iconReplaceDisabled)
                deleteCompletionConfirmations(ctx, row);
            else
                restoreCompletionConfirmations(ctx, row);

        } catch (CommandException | IOException e) {
            //TODO: write log
            e.printStackTrace();
        }
    }

    private void deleteCompletionConfirmations(DbContext ctx, ProductionCentre.Row row) throws CommandException, IOException {

        // Menu
        MenuBuilder<Integer> menuBuilder = new MenuBuilder<>(ctx, "Rückmeldungen");
        menuBuilder.addItem(1, "alle gebuchten Rückmeldungen rückgängig machen");
        menuBuilder.addItem(2, "abbruch");

        Integer choice = menuBuilder.show();
        if (choice == null)
            return;
        if (choice != 1)
            return;

        // Check workorders
        final String workordersID = row.getString("order");
        if (RecordUtils.isEmpty(workordersID))
            return;

        // Workorders number
        final WorkOrders workOrders = WorkordersUtils.loadByID(ctx, workordersID, true);
        if (workOrders == null)
            return;

        final String workOrdersIDNo = workOrders.getString("idno");

        final List<String> completionConfirmationsIDList = new ArrayList<>();

        // Get the list of all filed completion confirmations on this workorders
        final SelectionBuilder<CompletionConfirmations> selectionBuilder = SelectionBuilder.create(CompletionConfirmations.class);
        selectionBuilder.add(Conditions.between("idno"
                , String.format("%s%03d", workOrdersIDNo, 1)
                , String.format("%s%03d", workOrdersIDNo, 999)));
        selectionBuilder.add(Conditions.eq(CompletionConfirmations.META.yrmloe, false));
        selectionBuilder.setFilingMode(EnumFilingModeSelection.Filed);

        // for-each completion confirmations
        final Query<CompletionConfirmations> completionConfirmationsQuery = ctx.createQuery(selectionBuilder.build());
        final List<CompletionConfirmations> completionConfirmationsList = completionConfirmationsQuery.execute();
        for (CompletionConfirmations completionConfirmations : completionConfirmationsList) {

            // check completion confirmations type
            final EnumWorkOrderType completionConfirmationsType = completionConfirmations.getType();

            final boolean isNotCompletionConfirmations =
                    completionConfirmationsType != EnumWorkOrderType.CompletionConfirmation;

            if (isNotCompletionConfirmations)
                continue;

            // add ID to list
            final String completionConfirmationsID = completionConfirmations.getString("id");
            completionConfirmationsIDList.add(completionConfirmationsID);
        }

        // delete all completion confirmations
        final String dateTime = DateTimeFormatter.ofPattern("dd.MM.yy HH:mm").format(LocalDateTime.now());
        for (String completionConfirmationsID : completionConfirmationsIDList) {

            final CompletionConfirmations completionConfirmations = CompletionConfirmationsUtils.loadByID(ctx, completionConfirmationsID, true);

            final CompletionConfirmationsEditor completionConfirmationsEditor = completionConfirmations.createEditor();
            completionConfirmationsEditor.open(EditorAction.REVERSAL);
            completionConfirmationsEditor.setYrmloe(true);
            completionConfirmationsEditor.setComments(String.format("RMA %s", dateTime));
            completionConfirmationsEditor.commit();
        }

        // save list in workorders
        final StringBuilder stringBuilder = new StringBuilder();
        completionConfirmationsIDList.forEach(id -> stringBuilder.append(String.format("%s\n", id)));
        final StringReader stringReader = new StringReader(stringBuilder.toString());

        final WorkOrdersEditor workOrdersEditor = workOrders.createEditor();
        workOrdersEditor.open(EditorAction.MODIFY);
        workOrdersEditor.setYtnrmloe(stringReader);
        workOrdersEditor.commit();

        // update icon
        row.setString("yrmloe", "icon:replace");
    }

    private void restoreCompletionConfirmations(DbContext ctx, ProductionCentre.Row row) throws IOException, CommandException {

        // Menu
        MenuBuilder<Integer> menuBuilder = new MenuBuilder<>(ctx, "Rückmeldungen");
        menuBuilder.addItem(1, "alle gelöschten Rückmeldungen wieder herstellen");
        menuBuilder.addItem(2, "abbruch");

        Integer choice = menuBuilder.show();
        if (choice == null)
            return;
        if (choice != 1)
            return;

        // Check workorders
        final String workordersID = row.getString("order");
        if (RecordUtils.isEmpty(workordersID))
            return;

        // Workorders number
        final WorkOrders workOrders = WorkordersUtils.loadByID(ctx, workordersID, true);
        if (workOrders == null)
            return;

        // Read IDs to restore
        final StringWriter stringWriter = new StringWriter();
        workOrders.getYtnrmloe(stringWriter);

        // Split IDs
        final String[] ids = stringWriter.toString().split("\n");

        // for-each ID - restore
        final String dateTime = DateTimeFormatter.ofPattern("dd.MM.yy HH:mm").format(LocalDateTime.now());
        for (String id : ids) {

            // check id
            final String idTrim = id.trim();
            if (idTrim.isEmpty())
                continue;

            // check object completion confirmations
            final CompletionConfirmations completionConfirmationsToRestore =
                    CompletionConfirmationsUtils.loadByID(ctx, idTrim, true);
            if (completionConfirmationsToRestore == null)
                continue;

            final String lastReservationID = completionConfirmationsToRestore
                    .getString(CompletionConfirmations.META.lastReservation.getName());

            if (RecordUtils.isEmpty(lastReservationID))
                continue;

            final Reservations lastReservation = ReservationsUtils.loadByID(ctx, lastReservationID,true);
            if (lastReservation==null)
                continue;

            final String workOrdersWSlipID = lastReservation.getString(Reservations.META.workSlip.getName());
            if (RecordUtils.isEmpty(workOrdersWSlipID))
                continue;

            // create completion confirmations
            EditorCommand editorCommand = EditorCommandFactory.create(EditorAction.DONE, workOrdersWSlipID);

            final CompletionConfirmationsEditor editor = (CompletionConfirmationsEditor) ctx.openEditor(editorCommand);

            // ma
            editor.setString(CompletionConfirmationsEditor.META.employee.getName(),
                    completionConfirmationsToRestore.getString(CompletionConfirmationsEditor.META.employee.getName()));
            // mzeit
            editor.setMachineTimeHrs(completionConfirmationsToRestore.getMachineTimeHrs());
            // rzbuchen
            editor.setSetupTimeEntered(true);
            // sofort
            editor.setPrintImmediately(true);
            // bem
            editor.setComments(String.format("RMB %s", dateTime));
            editor.setYrmloe(false);
            // update first row
            final CompletionConfirmations.Row toRestoreRow = completionConfirmationsToRestore.table().getRow(1);
            final CompletionConfirmationsEditor.Row editorRow = editor.table().getRow(1);
            // gutmge
            editorRow.setConfYieldSU(toRestoreRow.getConfYieldSU());
            // tr
            editorRow.setSetupTime(toRestoreRow.getSetupTime());
            editor.commit();
        }

        // Delete IDs from workorders
        final WorkOrdersEditor workOrdersEditor = workOrders.createEditor();
        workOrdersEditor.open(EditorAction.MODIFY);
        workOrdersEditor.setYtnrmloe(new StringReader(""));
        workOrdersEditor.commit();

        // update icon
        row.setString("yrmloe", "icon:replace_disabled");
    }
}
